<?php


namespace App\Services;


use App\Events\NewsEvent;
use App\Events\ProductEvent;
use App\Models\News;
use App\Models\Product;
use App\Models\Subscription;
use App\Models\Worker;

class EventService
{
    /**
     * @param News $news
     */
    public function createNewsEvent(News $news): void
    {
        Subscription::select('user_id')
            ->where('news', true)
            ->groupBy('user_id')
            ->get()
            ->pluck('user_id')
            ->merge(Worker::select('user_id')
                ->where('company_id', $news->company_id)
                ->groupBy('user_id')
                ->get()
                ->pluck('user_id'))
            ->unique()
            ->each(static function (int $user_id) use ($news) {
                event(new NewsEvent($news, $user_id));
            });
    }

    /**
     * @param Product $product
     */
    public function createProductEvent(Product $product): void
    {
        Subscription::select('user_id')
            ->where('products', true)
            ->groupBy('user_id')
            ->get()
            ->pluck('user_id')
            ->merge(Worker::select('user_id')
                ->where('company_id', $product->company_id)
                ->groupBy('user_id')
                ->get()
                ->pluck('user_id'))
            ->unique()
            ->each(static function (int $user_id) use ($product) {
                event(new ProductEvent($product, $user_id));
            });
    }

}
