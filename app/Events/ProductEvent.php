<?php

namespace App\Events;

use App\Models\Product;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;

class ProductEvent implements ShouldBroadcastNow
{
    use InteractsWithSockets, SerializesModels;

//    use Dispatchable, Queueable;

    public Product $product;
    private int $userId;

    /**
     * ProductEvent constructor.
     * @param Product $product
     * @param int $userId
     */
    public function __construct(Product $product, int $userId)
    {
        $this->product = $product;
        $this->userId = $userId;
    }

    /**
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new channel('App.Models.User.' . $this->userId);
    }

    /**
     * @return string
     */
    public function broadcastAs(): string
    {
        return 'product';
    }
}
