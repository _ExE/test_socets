<?php

namespace App\Events;

use App\Models\News;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;

class NewsEvent implements ShouldBroadcastNow
{
    use InteractsWithSockets, SerializesModels;

//    use Dispatchable, Queueable;

    public News $news;
    private int $userId;

    /**
     * NewsEvent constructor.
     * @param News $news
     * @param int $userId
     */
    public function __construct(News $news, int $userId)
    {
        $this->news = $news;
        $this->userId = $userId;
    }

    /**
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new channel('App.Models.User.' . $this->userId);
    }

    /**
     * @return string
     */
    public function broadcastAs(): string
    {
        return 'news';
    }
}
