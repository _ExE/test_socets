<?php

namespace App\Observers;

use App\Models\Product;
use App\Services\EventService;

class ProductObserver
{
    private EventService $eventService;

    /**
     * ProductObserver constructor.
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }


    /**
     * Handle the Product "created" event.
     *
     * @param Product $product
     * @return void
     */
    public function created(Product $product): void
    {
        $this->eventService->createProductEvent($product);
    }

}
