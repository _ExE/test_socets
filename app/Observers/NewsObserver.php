<?php

namespace App\Observers;

use App\Models\News;
use App\Services\EventService;

class NewsObserver
{
    private EventService $eventService;

    /**
     * NewsObserver constructor.
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Handle the News "created" event.
     *
     * @param News $news
     * @return void
     */
    public function created(News $news): void
    {
        $this->eventService->createNewsEvent($news);
    }

}
