<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\News;
use Illuminate\Console\Command;

class createNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:createNews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create News';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $company = Company::factory()->hasWorkers(1)->create();
        News::create([
            'company_id' => $company->id,
            'title' => 'Новость от ' . now()->toString(),
            'content' => 'Содержание новости от ' . now()->toString(),
        ]);
    }
}
