<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Product;
use Illuminate\Console\Command;

class createProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:createProduct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create new Product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $company = Company::factory()->hasWorkers(1)->create();
        Product::create([
            'company_id' => $company->id,
            'name' => 'Продукт от ' . now()->toString(),
            'price' => 100500,
        ]);
    }
}
