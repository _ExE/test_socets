<?php

use App\Models\News;
use App\Models\Product;
use App\Models\Subscription;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
});

//Broadcast::channel('news.{newsId}', function ($user, $newsId) {
//    if (News::findOrNew($newsId)->isWorkerSubscription($user->id)) {
//        return true;
//    }
//    return Subscription::where('user_id', $user->id)
//            ->where('news', true)
//            ->count() === 1;
//});
//
//Broadcast::channel('product.{productId}', function ($user, $productId) {
//    if (Product::findOrNew($productId)->isWorkerSubscription($user->id)) {
//        return true;
//    }
//    return Subscription::where('user_id', $user->id)
//            ->where('product', true)
//            ->count() === 1;
//});
